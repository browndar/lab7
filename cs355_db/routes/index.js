var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'CS355', subtitle: ' Lab 7' });
});

/* GET Template Example */
router.get('/template_example',function(req,res,next){
    res.render('template_example',{f_name: req.query.f_name,l_name:req.query.l_name})
});

/* GET Resume */
router.get('/resume',function(req,res,next){
    res.render('resume_view',{first_name: req.query.first_name,last_name:req.query.last_name,major_name: req.query.major_name})
});


module.exports = router;
